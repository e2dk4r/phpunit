<?php
declare(strict_types=1);


namespace App\Catalog\Handler;

use App\Catalog\Value\Product;
use Laminas\Diactoros\Response\JsonResponse;

abstract class ProductHandler {
  
  protected function createProductsResponse(array $products): JsonResponse {
    return new JsonResponse(array_map(static fn(Product $product) => $product->toArray(), $products));
  }
  
}