<?php
declare(strict_types=1);

namespace App\Catalog\Value;

final class Discount {
  private const TYPE_AMOUNT = 'amount';
  private const TYPE_PERCENT = 'percent';
  
  private string $type;
  private float $value;
  
  private function __construct(string $type, float $value) {
    $this->type = $type;
    $this->value = $value;
  }
  
  public static function fromAmount(int $amount): self {
    return new self(self::TYPE_AMOUNT, $amount);
  }
  
  public static function fromPercent(float $amount): self {
    return new self(self::TYPE_PERCENT, $amount * 100);
  }
  
  public function getDiscountAmountForPrice(Amount $priceAmount): Amount {
    $discountAmount = match ($this->type) {
      self::TYPE_AMOUNT => (int)$this->value,
      self::TYPE_PERCENT => (int)($priceAmount->get_cents() / 100 * $this->value),
    };
    
    return new Amount($discountAmount);
  }
}