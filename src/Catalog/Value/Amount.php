<?php
declare(strict_types=1);

namespace App\Catalog\Value;

use App\Catalog\Exceptions\AmountBelowZeroException;

final class Amount {
  private int $cents;
  
  public function __construct(int $cents) {
    if ($cents < 0)
      throw AmountBelowZeroException::fromInt($cents);
    
    $this->cents = $cents;
  }
  
  /**
   * @return int
   */
  public function get_cents(): int {
    return $this->cents;
  }
}