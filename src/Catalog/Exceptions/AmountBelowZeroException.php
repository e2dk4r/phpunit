<?php
declare(strict_types=1);

namespace App\Catalog\Exceptions;

use InvalidArgumentException;

final class AmountBelowZeroException extends InvalidArgumentException {

  public static function fromInt(int $amountInCents): self {
    return new self(sprintf('Money amount cannot be below zero, %d given' , $amountInCents));
  }
}