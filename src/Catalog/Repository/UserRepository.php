<?php
declare(strict_types=1);

namespace App\Catalog\Repository;

interface UserRepository {
  
  public function findUsersInArea(string $area): array;
  public function findUsersInterestedInArtist(string $artistName): array;
  
}