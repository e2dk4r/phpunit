<?php
declare(strict_types=1);

namespace App;

use App\Catalog\Handler\ProductListHandler;
use App\Catalog\Repository\DoctrineProductRepository;
use App\Catalog\Repository\ProductRepository;
use App\Catalog\SearchAnalytics\FilesystemSearchAnalytics;
use App\Catalog\SearchAnalytics\SearchAnalytics;
use App\Clock\Clock;
use App\Clock\DateTimeClock;
use DI;
use DI\Container;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Mysqli\Driver;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use Psr\Container\ContainerInterface;

final class DependencyInjection {
  
  public function createContainer(): Container {
    $di = new Container();
    
    // doctrine
    $di->set(Connection::class, DI\factory(function () {
      return new Connection(
        //[
        //  'host' => 'localhost',
        //  'port' => '3306',
        //  'user' => 'e2dk4r',
        //  'password' => 'Y#iFkmJ[#(L59L6>+Nhei("3/&f,j!<S)oS-h^)eF&<BayYVrh;kH2>cuZzREo%#',
        //  'dbname' => 'php_unit_test',
        //],
        [
          'host' =>     'mariadb.internal',
          'port' =>     '3306',
          'user' =>     'pluralsight',
          'password' => 'pluralsight',
          'dbname' =>   'pluralsight',
        ],
        new Driver()
      );
    }));
    $di->set(EntityManagerInterface::class, DI\factory(function (ContainerInterface $di): EntityManager {
      $config = Setup::createAnnotationMetadataConfiguration(
        [ __DIR__ . '/src/Entity' ],
        false,
        null,
        null,
        false
      );
      
      return EntityManager::create(
        $di->get(Connection::class),
        $config
      );
    }));
    
    $di->set(ProductRepository::class, DI\autowire(DoctrineProductRepository::class));
    $di->set(SearchAnalytics::class, DI\autowire(FilesystemSearchAnalytics::class));
    $di->set(ProductListHandler::class, DI\autowire(ProductListHandler::class));
    $di->set(Clock::class, DI\autowire(DateTimeClock::class));
    
    return $di;
  }
  
}