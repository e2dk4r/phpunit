<?php
declare(strict_types=1);

namespace App\AmountCalculator;

use App\Catalog\Value\Amount;

final class AmountCalculator {
  
  
  /**
   * @param Amount $amount
   * @param Operation[]  $operations
   * @return Amount
   */
  public function getResult(Amount $amount, array $operations): Amount {
    foreach ($operations as $operation) {
      $amount = $operation->applyTo($amount);
    }
    
    return $amount;
  }
 
}