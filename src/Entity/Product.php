<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Entity
 * @ORM\Table(name="product", indexes={@ORM\Index(name="idx_name", columns={"name"})})
 */
class Product {
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
   */
  public int $id;
  
  /**
   * @ORM\Column(name="name", type="string", length=255, nullable=false)
   */
  public string $name;
  
  /**
   * @ORM\Column(name="cost", type="integer", nullable=false, options={"unsigned"=true})
   */
  public int $cost;
  
  /**
   * @ORM\Column(name="markup", type="integer", nullable=false, options={"unsigned"=true})
   */
  public int $markup;
  
  /**
   * @var Discount[]
   *
   * @ORM\OneToMany(targetEntity="Discount", mappedBy="product")
   */
  public $discounts;
}