<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Discount
 *
 * @ORM\Entity
 * @ORM\Table(name="discount", indexes={@ORM\Index(name="fk_product", columns={"product_id"})})
 */
class Discount {
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
   */
  public int $id;
  
  /**
   * @ORM\Column(name="name", type="string", length=255, nullable=false)
   */
  public string $name;
  
  /**
   * @ORM\Column(name="type", type="string", length=10, nullable=false)
   */
  public string $type;
  
  /**
   * @ORM\Column(name="value", type="integer", nullable=false, options={"unsigned"=true})
   */
  public int $value;
  
  /**
   * @var Product
   *
   * @ORM\ManyToOne(targetEntity="Product")
   * @ORM\JoinColumns({
   *  @ORM\JoinColumn(name="product_id", referencedColumnName="id")
   * })
   */
  public $product;
}