<?php

namespace App\Clock;

use DateTimeImmutable;

interface Clock {
  
  public function now(): DateTimeImmutable;
  
}
