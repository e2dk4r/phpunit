CREATE TABLE product (
    id     INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name   VARCHAR(255)    NOT NULL,
    cost   INT(8) UNSIGNED NOT NULL,
    markup INT(8) UNSIGNED NOT NULL
);

CREATE TABLE discount (
    id         INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    product_id INT UNSIGNED        NOT NULL,
    name       VARCHAR(255)        NOT NULL,
    type       TINYINT(1) UNSIGNED NOT NULL,
    value      INT(8) UNSIGNED     NOT NULL,
    CONSTRAINT fk_product
        FOREIGN KEY (product_id) REFERENCES product (id)
            ON DELETE CASCADE
);

CREATE INDEX idx_name
    ON product (name);