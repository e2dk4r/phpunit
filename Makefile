TZ        := UTC
LC_ALL    := C.UTF-8
SHELL     := bash
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
.RECIPEPREFIX = >

PROJECT := phpunit
COMPOSE := docker-compose --project-name $(PROJECT)
BUILD   := $(COMPOSE) build --no-cache
UP      := $(COMPOSE) up
DOWN    := $(COMPOSE) down
RUN     := $(COMPOSE) run --rm
RM      := $(COMPOSE) rm --force --stop

all: up

build:
> $(BUILD)

up:
> $(UP)

down:
> $(DOWN)
> $(RM)

test:
> $(RUN) php sh -c vendor/bin/phpunit -c phpunit.xml --coverage-text --colors=never
