<?php
declare(strict_types=1);

namespace App\Tests;

use App\DependencyInjection;
use DI\Container;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ForwardCompatibility\Result;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

abstract class IntegrationTestCase extends TestCase {
  protected Container $diContainer;
  protected ContainerInterface $container;
  
  protected function initContainer() {
    $di = new DependencyInjection();
    $this->diContainer = $di->createContainer();
    $this->container = $this->diContainer;
  }
  
  protected function resetDatabase(): void {
    $this->importFixture(__DIR__ . '/fixtures/truncate.sql');
  }
  
  protected function insertRecord(string $table, array $data): int {
    return $this->getConnection()->insert($table, $data);
  }
  
  protected function updateRecord(string $table, array $data, array $where): int {
    return $this->getConnection()->update($table, $data, $where);
  }
  
  protected function getConnection() : Connection {
    return $this->container
      ->get(EntityManagerInterface::class)
      ->getConnection();
  }
  
  protected function importFixture(string $fixtureFile): void {
    $fp = fopen($fixtureFile, 'r');
    $connection = $this->getConnection();
    
    while (!feof($fp)) {
      $sql = fgets($fp);
      if (trim($sql) !== '') {
        $result = $this->getConnection()->executeQuery($sql);
        $result->free();
      }
    }
    
    fclose($fp);
    
  }
}