<?php
declare(strict_types=1);

namespace App\Tests\AmountCalculator\Operation;

use App\AmountCalculator\Operation\MarkupOperation;
use App\Catalog\Value\Amount;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use stdClass;

final class MarkupOperationTest extends TestCase {
  
  /**
   * @test
   */
  public function applyTo_WithMarkup_AddsPercentMarkup(): void {
    $operation = new MarkupOperation(.25);
    $amount = new Amount(100);
    $amountWithMarkup = $operation->applyTo($amount);
    
    self::assertEquals(125, $amountWithMarkup->get_cents());
  }
  
  /**
   * @test
   * @dataProvider getInvalidMarkupValues
   * @param $markup
   */
  public function constructor_WithInvalidMarkupValues($markup): void {
    $this->expectException(InvalidArgumentException::class);
    new MarkupOperation($markup);
  }
  
  public function getInvalidMarkupValues(): array {
    return [
      'int' => [1],
      'string' => ['0.25'],
      'boolean' => [true],
      'array' => [[]],
      'null' => [null],
      'object' => [new stdClass()],
    ];
  }
}