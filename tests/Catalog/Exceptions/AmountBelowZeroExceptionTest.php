<?php

namespace App\Tests\Catalog\Exceptions;

use App\Catalog\Exceptions\AmountBelowZeroException;
use PHPUnit\Framework\TestCase;

class AmountBelowZeroExceptionTest extends TestCase {
  
  /** @test */
  public function fromInt_WithCents_SetsMessageContainingCents(): void {
    $exception = AmountBelowZeroException::fromInt(-10);
    
    self::assertEquals(
      'Money amount cannot be below zero, -10 given',
      $exception->getMessage()
    );
  }
}
