<?php

namespace App\Tests\Catalog\Repository;

use App\Catalog\Repository\DoctrineProductRepository;
use App\Catalog\Value\Amount;
use App\Catalog\Value\Product;
use App\Tests\IntegrationTestCase;
use PHPUnit\Framework\TestCase;

/** @covers \App\Catalog\Repository\DoctrineProductRepository */
class DoctrineProductRepositoryTest extends IntegrationTestCase {
  /** @test */
  public function findProducts_WithoutDiscount_ReturnsFullPriceProducts(): void {
    $this->initContainer();
    $this->resetDatabase();
    $this->insertRecord('product', [
      'name'   => 'Concert',
      'cost'   => 1000,
      'markup' => 10,
    ]);
    
    /** @var DoctrineProductRepository $repository */
    $repository = $this->container->get(DoctrineProductRepository::class);
    
    self::assertEquals(
      [
        new Product('Concert', new Amount(1100)),
      ],
      $repository->findProducts()
    );
  }
}
