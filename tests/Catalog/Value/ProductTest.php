<?php
declare(strict_types=1);

namespace App\Tests\Catalog\Value;

use App\Catalog\Value\Amount;
use App\Catalog\Value\Product;
use PHPUnit\Framework\TestCase;

final class ProductTest extends TestCase {
  const NAME = 'product';
  const PRICE = 100;
  private Product $product;
  
  protected function setUp(): void {
    $this->product = new Product(
      self::NAME,
      new Amount(self::PRICE)
    );
  }
  
  /** @test */
  public function getName_ReturnsString() : void {
    self::assertEquals(
      self::NAME,
      $this->product->getName()
    );
  }
  
  /** @test */
  public function getPrice_ReturnsAmount() : void {
    self::assertEquals(
      new Amount(self::PRICE),
      $this->product->getPrice()
    );
  }
  
  /** @test */
  public function toArray_ReturnsFormattedValues() : void {
    self::assertEquals(
      [
        'name' => self::NAME,
        'price' => '1.00',
      ],
      $this->product->toArray()
    );
  }
}