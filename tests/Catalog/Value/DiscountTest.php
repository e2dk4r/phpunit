<?php

namespace App\Tests\Catalog\Value;

use App\Catalog\Value\Amount;
use App\Catalog\Value\Discount;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Catalog\Value\Discount
 */
class DiscountTest extends TestCase {
  const AMOUNT = 100;
  const DISCOUNT = 60;
  const PERCENT_DISCOUNT = .60;
  
  /**
   * @test
  */
  public function getDiscountAmountForPrice_FromAmount_WillReturnDiscountedAmount() : void {
    $amount = new Amount(self::AMOUNT);
    $discount = Discount::fromAmount(self::DISCOUNT);
    
    self::assertEquals(
      new Amount(self::DISCOUNT),
      $discount->getDiscountAmountForPrice($amount)
    );
  }
  
  /**
   * @test
   */
  public function getDiscountAmountForPrice_FromPercent_WillReturnDiscountedAmount() : void {
    $amount = new Amount(self::AMOUNT);
    $discount = Discount::fromPercent(self::PERCENT_DISCOUNT);
    
    self::assertEquals(
      new Amount(self::DISCOUNT),
      $discount->getDiscountAmountForPrice($amount)
    );
  }
}
