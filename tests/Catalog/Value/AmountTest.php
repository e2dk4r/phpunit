<?php
declare(strict_types=1);

use App\Catalog\Exceptions\AmountBelowZeroException;
use App\Catalog\Value\Amount;
use PHPUnit\Framework\TestCase;

final class AmountTest extends TestCase {
  
  /** @test */
  public function getCents_WithValidCents_ReturnsUnchangedCents(): void {
    $amount = new Amount(1000);
    $cents = $amount->get_cents();
    self::assertEquals(1000, $cents);
  }
  
  /** @test */
  public function constructor_WithNegativeCents_ThrowsException(): void {
    $this->expectException(AmountBelowZeroException::class);
    new Amount(-1);
  }
  
}