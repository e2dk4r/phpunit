<?php

namespace App\Tests\Catalog\SearchAnalytics;

use App\Catalog\SearchAnalytics\FilesystemSearchAnalytics;
use App\Catalog\SearchAnalytics\SearchAnalytics;
use App\Clock\Clock;
use App\Tests\IntegrationTestCase;
use DateTimeImmutable;
use DateTimeZone;
use DI;

/** @covers \App\Catalog\SearchAnalytics\FilesystemSearchAnalytics */
class FilesystemSearchAnalyticsTest extends IntegrationTestCase {
  const FILE_PATH = __DIR__ . '/../../../var/search_analytics.csv';
  
  /** @test */
  public function track_WithTimestamp_AsJson_ToCSV(): void {
    /** CLEANUP RESOURCES */
    unlink(self::FILE_PATH);
    
    /** ASSIGN */
    $this->initContainer();
    
    // configure clock
    $this->diContainer->set(Clock::class, DI\factory(function () {
      $clock = self::createStub(Clock::class);
      $clock
        ->method('now')
        ->willReturn(
          (new DateTimeImmutable())
            ->setDate(2021, 07, 01)
            ->setTime(12, 30, 10)
            ->setTimezone(new DateTimeZone('UTC'))
        );
      
      return $clock;
    }));
    
    /** @var SearchAnalytics $searchAnalytics */
    $searchAnalytics = $this->container->get(FilesystemSearchAnalytics::class);
  
    /** ACT */
    $searchAnalytics->track(['price' => null, 'name' => null]);
    
    /** ASSERT */
    self::assertEquals(
      '"2021-07-01T12:30:10+00:00","{\"price\":null,\"name\":null}"',
      file_get_contents(self::FILE_PATH)
    );
  }
}
