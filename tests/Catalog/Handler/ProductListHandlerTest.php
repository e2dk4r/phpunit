<?php

namespace App\Tests\Catalog\Handler;

use App\Catalog\Handler\ProductListHandler;
use App\Catalog\Repository\ProductRepository;
use App\Catalog\SearchAnalytics\SearchAnalytics;
use App\Catalog\Value\Amount;
use App\Catalog\Value\Product;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;

class ProductListHandlerTest extends TestCase {
  const CONCERT_NAME = 'concert 1';
  const CONCERT_PRICE_STRING = '10.00';
  const CONCERT_PRICE_CENTS = 1000;
  
  /** @test */
  public function handle_WillReturnJsonOutput() :void {
    $request = $this->createMock(ServerRequestInterface::class);
    $searchAnalytics = $this->createMock(SearchAnalytics::class);
    $productRepository = $this->createStub(ProductRepository::class);
    $productRepository
      ->method('findProducts')
      ->willReturn([
        new Product(self::CONCERT_NAME, new Amount(self::CONCERT_PRICE_CENTS)),
      ]);
  
    $handler = new ProductListHandler($productRepository, $searchAnalytics);
    $response = $handler->handle($request);
    $json = json_decode($response->getBody()->getContents(), true);
    
    self::assertEquals(
      [
        [
          'name' => self::CONCERT_NAME,
          'price' => self::CONCERT_PRICE_STRING
        ]
      ],
      $json
    );
  }
  
  /**
   * @test ensure product search properly tracked with search analytics
   */
  public function handle_TracksSearchWithSearchAnalytics() {
    $request = $this->createMock(ServerRequestInterface::class);
    $productRepository = $this->createMock(ProductRepository::class);
    $searchAnalytics = $this->createMock(SearchAnalytics::class);
    $searchAnalytics
      ->expects($this->once())
      ->method('track')
      ->with(
        [
          'price' => null,
          'name' => null,
        ]
      );
    
    $handler = new ProductListHandler($productRepository, $searchAnalytics);
    
    $handler->handle($request);
  }
}
